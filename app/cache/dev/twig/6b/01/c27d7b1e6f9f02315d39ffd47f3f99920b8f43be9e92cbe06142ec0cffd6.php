<?php

/* AcmeDemoBundle:Welcome:sidebar.html.twig */
class __TwigTemplate_6b01c27d7b1e6f9f02315d39ffd47f3f99920b8f43be9e92cbe06142ec0cffd6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

<ul id=\"mainmenu\">
    <li>
        <a href=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("_welcome");
        echo "\">Strona główna</a>
    </li>
    ";
        // line 7
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 8
            echo "    <li>
     <a href=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("index_category", array("id" => $this->getAttribute((isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "title"), "html", null, true);
            echo "</a>
    </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "    <li>
     <a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("_demo_contact");
        echo "\">Kontakt</a>
    </li>
</ul>

";
    }

    public function getTemplateName()
    {
        return "AcmeDemoBundle:Welcome:sidebar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 13,  48 => 12,  37 => 9,  34 => 8,  30 => 7,  25 => 5,  19 => 1,);
    }
}
