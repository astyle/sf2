<?php

/* AcmeDemoBundle:Welcome:categorynews.html.twig */
class __TwigTemplate_b2815ef3521041588b6554f22e2acde17f3b0e3055bee1164ec02a82324699b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AcmeDemoBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AcmeDemoBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Aktualności kategorii - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "title"), "html", null, true);
    }

    // line 5
    public function block_content_header($context, array $blocks = array())
    {
        echo "";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
Ostatnie aktualności dla kategorii ";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "title"), "html", null, true);
        echo "

";
        // line 11
        if ((isset($context["news"]) ? $context["news"] : $this->getContext($context, "news"))) {
            // line 12
            echo "<ul>
    ";
            // line 13
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["news"]) ? $context["news"] : $this->getContext($context, "news")));
            foreach ($context['_seq'] as $context["_key"] => $context["onenew"]) {
                // line 14
                echo "    <li>
     
     <a href=\"";
                // line 16
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("index_show", array("id" => $this->getAttribute((isset($context["onenew"]) ? $context["onenew"] : $this->getContext($context, "onenew")), "id"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["onenew"]) ? $context["onenew"] : $this->getContext($context, "onenew")), "title"), "html", null, true);
                echo "</a><br>
     ";
                // line 17
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["onenew"]) ? $context["onenew"] : $this->getContext($context, "onenew")), "description"), "html", null, true);
                echo "<br>     
    </li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['onenew'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "</ul>

";
        } else {
            // line 23
            echo "
<br>Brak aktualności w tej kategorii

";
        }
        // line 27
        echo "
";
    }

    public function getTemplateName()
    {
        return "AcmeDemoBundle:Welcome:categorynews.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 27,  87 => 23,  82 => 20,  73 => 17,  67 => 16,  63 => 14,  59 => 13,  56 => 12,  54 => 11,  49 => 9,  46 => 8,  43 => 7,  37 => 5,  30 => 3,);
    }
}
