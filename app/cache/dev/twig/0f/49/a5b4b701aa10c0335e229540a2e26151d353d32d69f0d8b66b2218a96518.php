<?php

/* AcmeDemoBundle:Welcome:authornews.html.twig */
class __TwigTemplate_0f49a5b4b701aa10c0335e229540a2e26151d353d32d69f0d8b66b2218a96518 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AcmeDemoBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AcmeDemoBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Aktualności autora - ";
        echo twig_escape_filter($this->env, (isset($context["author"]) ? $context["author"] : $this->getContext($context, "author")), "html", null, true);
    }

    // line 5
    public function block_content_header($context, array $blocks = array())
    {
        echo "";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
Aktualności autora: ";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["author"]) ? $context["author"] : $this->getContext($context, "author")), "html", null, true);
        echo "
<ul>
    ";
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["news"]) ? $context["news"] : $this->getContext($context, "news")));
        foreach ($context['_seq'] as $context["_key"] => $context["onenew"]) {
            // line 12
            echo "    <li>
     
     <a href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("index_show", array("id" => $this->getAttribute((isset($context["onenew"]) ? $context["onenew"] : $this->getContext($context, "onenew")), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["onenew"]) ? $context["onenew"] : $this->getContext($context, "onenew")), "title"), "html", null, true);
            echo "</a><br>
     ";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["onenew"]) ? $context["onenew"] : $this->getContext($context, "onenew")), "description"), "html", null, true);
            echo "<br>     
    </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['onenew'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "</ul>

";
    }

    public function getTemplateName()
    {
        return "AcmeDemoBundle:Welcome:authornews.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 18,  68 => 15,  62 => 14,  58 => 12,  54 => 11,  49 => 9,  46 => 8,  43 => 7,  37 => 5,  30 => 3,);
    }
}
