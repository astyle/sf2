<?php

/* AcmeDemoBundle:Welcome:index.html.twig */
class __TwigTemplate_edac8a891d308181987df56c611b534100d2247e150e5c63249373305797f0a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("AcmeDemoBundle::layout.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AcmeDemoBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Strona główna";
    }

    // line 5
    public function block_content_header($context, array $blocks = array())
    {
        echo "";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
Ostatnie aktualności
<ul>
    ";
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["news"]) ? $context["news"] : $this->getContext($context, "news")));
        foreach ($context['_seq'] as $context["_key"] => $context["onenew"]) {
            // line 12
            echo "    <li>     
     <a href=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("index_show", array("id" => $this->getAttribute((isset($context["onenew"]) ? $context["onenew"] : $this->getContext($context, "onenew")), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["onenew"]) ? $context["onenew"] : $this->getContext($context, "onenew")), "title"), "html", null, true);
            echo "</a><br>
     ";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["onenew"]) ? $context["onenew"] : $this->getContext($context, "onenew")), "description"), "html", null, true);
            echo "<br>     
    </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['onenew'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "</ul>

";
    }

    public function getTemplateName()
    {
        return "AcmeDemoBundle:Welcome:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 17,  63 => 14,  57 => 13,  54 => 12,  50 => 11,  45 => 8,  42 => 7,  36 => 5,  30 => 3,);
    }
}
