<?php

/* AcmeDemoBundle::layout.html.twig */
class __TwigTemplate_1a00f4e7d3dc7b1213548d8935b65f6c455f5832c6d2694df79d793185766172 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\"/>
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/css/structure.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/css/body.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        ";
        // line 9
        $this->displayBlock('head', $context, $blocks);
        // line 13
        echo "    </head>
    <body>
        <div id=\"content\">
            <div class=\"header clear-fix\">
                
                
            </div>
            <div id=\"sidebar\">
    ";
        // line 21
        echo $this->env->getExtension('actions')->renderUri($this->env->getExtension('routing')->getUrl("_sidebar"), array());
        // line 22
        echo "                </div>
                <div class=\"sf-reset\">
                ";
        // line 24
        $this->displayBlock('body', $context, $blocks);
        // line 50
        echo "                            </div>
                        </div>
                    </body>
                </html>







";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "Demo Bundle";
    }

    // line 9
    public function block_head($context, array $blocks = array())
    {
        // line 10
        echo "        <link rel=\"icon\" sizes=\"16x16\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/acmedemo/css/demo.css"), "html", null, true);
        echo "\" />
";
    }

    // line 24
    public function block_body($context, array $blocks = array())
    {
        // line 25
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 26
            echo "                        <div class=\"flash-message\">
                            <em>Notice</em>: ";
            // line 27
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
                        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "
    ";
        // line 31
        $this->displayBlock('content_header', $context, $blocks);
        // line 38
        echo "
                            <div class=\"block\">
        ";
        // line 40
        $this->displayBlock('content', $context, $blocks);
        // line 41
        echo "                                </div>

    ";
        // line 43
        if (array_key_exists("code", $context)) {
            // line 44
            echo "                                <h2>Code behind this page</h2>
                                <div class=\"block\">
                                    <div class=\"symfony-content\">";
            // line 46
            echo (isset($context["code"]) ? $context["code"] : $this->getContext($context, "code"));
            echo "</div>
                                </div>
    ";
        }
    }

    // line 31
    public function block_content_header($context, array $blocks = array())
    {
        // line 32
        echo "                        <ul id=\"menu\">
            
                            </ul>

                            <div style=\"clear: both\"></div>
    ";
    }

    // line 40
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "AcmeDemoBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 40,  153 => 32,  150 => 31,  142 => 46,  138 => 44,  136 => 43,  132 => 41,  130 => 40,  126 => 38,  124 => 31,  121 => 30,  112 => 27,  109 => 26,  104 => 25,  101 => 24,  95 => 11,  90 => 10,  87 => 9,  81 => 6,  66 => 50,  64 => 24,  60 => 22,  58 => 21,  48 => 13,  46 => 9,  42 => 8,  38 => 7,  34 => 6,  29 => 4,  24 => 1,);
    }
}
