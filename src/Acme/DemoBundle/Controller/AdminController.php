<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class AdminController extends Controller
{
    
    public function indexAction()
    {
        /*
         * The action's view can be rendered using render() method
         * or @Template annotation as demonstrated in DemoController.
         *
         */
        
        
        
        return $this->render('AcmeDemoBundle:Admin:index.html.twig');
    }
    
    
   
    public function sidebarAction(){
        
        
       return $this->render('AcmeDemoBundle:Admin:sidebar.html.twig'); 
    }
    
}
