<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller
{   
    
    public $limit = 1;
    private $offset;
    public function __construct() {
       
        $this->offset = $this->limit*(Request::createFromGlobals()->get('p', 1)-1);
        
    }
    
    /**
     * 
     * @Template("AcmeDemoBundle:Welcome:index.html.twig")
     */
    public function indexAction()
    {
        
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AcmeDemoBundle:News')->findBy(array(), array(), 5);
        
        return array(
            'news' => $entities,
        );
        
    }
    
    /**
     * @Route("/", name="sidebar")
     * @Template("AcmeDemoBundle:Welcome:sidebar.html.twig")
     */
    public function sidebarAction(){
        
        
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AcmeDemoBundle:Category')->findAll();
        
        $sites = $em->getRepository('AcmeDemoBundle:Sites')->findAll();

        
        return array(
            'categories' => $entities,
            'sites' => $sites,
        );
    }
    
    /**
     * @Route("/", name="leftbar")
     * @Template("AcmeDemoBundle:Welcome:leftbar.html.twig")
     */
    public function leftbarAction(){
        
        
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AcmeDemoBundle:Category')->findAll();
        
        $sites = $em->getRepository('AcmeDemoBundle:Sites')->findAll();
        
        $authors = $em->getRepository('AcmeDemoBundle:User')->findAll();

        
        return array(
            'categories' => $entities,
            'authors' => $authors,
        );
    }
    
    /**
     * @Route("news/{id}", name="index_show")
     * @Template("AcmeDemoBundle:Welcome:show.html.twig")
     */
    public function newsAction($id){
        
        
        $em = $this->getDoctrine()->getManager();
        
        $entities = $em->getRepository('AcmeDemoBundle:News')->find($id);       
        if(!$entities) {
            throw $this->createNotFoundException('Podana strona nie istnieje');
        }
        
        return array(
            'news' => $entities,
        );
    }
    
    
    /**
     * @Route("category/{id}", name="index_category")
     * @Template("AcmeDemoBundle:Welcome:categorynews.html.twig")
     */
    public function categoryNewsAction($id){
         
        
        $em = $this->getDoctrine()->getManager();
        
        $category = $em->getRepository('AcmeDemoBundle:Category')->find($id);
        if(!$category) {
            throw $this->createNotFoundException('Podana strona nie istnieje');
        }
        
        $entities = $em->getRepository('AcmeDemoBundle:News')->findBy(array('category'=>$id),array(),$this->limit,$this->offset);
        //$category->getNews();
        
        $count = $em->createQuery("Select count(a.id) from AcmeDemoBundle:News a where a.category = '{$id}'")->getSingleResult();
        
        return array(
            'news' => $entities,
            'category' => $category,
            'count'=>$count[1],
            'per_page'=>$this->limit
        );
    }
    
    
    /**
     * @Route("sites/{id}", name="index_sites")
     * @Template("AcmeDemoBundle:Welcome:sites.html.twig")
     */
    public function sitesAction($id){
         
        
        $em = $this->getDoctrine()->getManager();
        
        $site = $em->getRepository('AcmeDemoBundle:Sites')->find($id);
        if(!$site) {
            throw $this->createNotFoundException('Podana strona nie istnieje');
        }
        
        
        return array(
            'site' => $site
        );
    }
    
    /**
     * @Route("author/{id}", name="index_authors")
     * @Template("AcmeDemoBundle:Welcome:authornews.html.twig")
     */
    public function authorNewsAction($id){     
        
        $em = $this->getDoctrine()->getManager();
        
        $author = $em->getRepository('AcmeDemoBundle:User')->find($id);        
        if(!$author) {
            throw $this->createNotFoundException('Podana strona nie istnieje');
        }
        
        $entities = $em->getRepository('AcmeDemoBundle:News')->findBy(array('user'=>$id),array(),$this->limit,$this->offset);
        
        $count = $em->createQuery("Select count(a.id) from AcmeDemoBundle:News a where a.user = '{$id}'")->getSingleResult();
               
        return array(
            'news' => $entities,
            'author' => $author,
            'count'=>$count[1],
            'per_page'=>$this->limit
        );
    }
    
    
}
