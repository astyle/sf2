<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class PagginatorController extends Controller
{   
    private $show_buttons = 11;
    
    /**
     * 
     * @Template("AcmeDemoBundle:Pagginator:index.html.php")
     */
    public function indexAction($count, $perpage, $show_buttons=false)
    {   
        if($show_buttons) $this->show_buttons = (int)$show_buttons;
        
        $request = Request::createFromGlobals();
        $p = $request->get('p', 1);
        
        $ile = ceil($count/$perpage);
        
        
        $ret = array(
            'p'=>$p,
            'link'=>'',
            'ile'=>$ile,
            'showButtons'=>$this->show_buttons       
        );
        
        return $ret;
    }
}
