<?php

namespace Acme\DemoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', 'email',array('label'  => 'Twój email'));
        $builder->add('message', 'textarea',array('label'  => 'Wiadomość'));
        $builder->add('send', 'submit',array('label'  => 'Wyślij'));
    }

    public function getName()
    {
        return 'contact';
    }
}
