<?php

namespace Acme\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="username", message="Username already taken")
 * @UniqueEntity(fields="nick", message="Nick already taken")
 */
class User implements UserInterface, \Serializable {
    
    
    private $all_roles = array(
        'ROLE_ADMIN'=>'Administrator',//administrator role
        'ROLE_USER'=>'Redaktor'// editor role
        );
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;
    
    /**
     * @ORM\Column(type="string", length=25, unique=false)
     */
    private $name;
    
    /**
     * @ORM\Column(type="string", length=25, unique=false)
     */
    private $surname;
    
    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $nick;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $salt;
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=60, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    public function __construct() {
        $this->isActive = true;
        $this->salt = '';
    } 
    
    
     /**
     * @inheritDoc
     */
    public function getId() {
        return $this->id;
    }
    public function setUsername($username) {
        $this->username = $username;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setSurname($surname) {
        $this->surname = $surname;
    }

    public function setNick($nick) {
        $this->nick = $nick;
    }

    public function setSalt($salt) {
        $this->salt = $salt;
    }
    
    public function setRole($role) {
        $this->role = $role;
    }

    public function setPassword($password) {
        $this->password = md5($password);
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setIsActive($isActive) {
        $this->isActive = $isActive;
    }

        /**
     * @inheritDoc
     */
    public function getUsername() {
        return $this->username;
    }
    
     /**
     * @inheritDoc
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * @inheritDoc
     */
    public function getSurname() {
        return $this->surname;
    }
    
    /**
     * @inheritDoc
     */
    public function getNick() {
        return $this->nick;
    }
    
    /**
     * @inheritDoc
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @inheritDoc
     */
    public function getSalt() {
        return '';
        //return $this->salt;
    }

    /**
     * @inheritDoc
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getRole() {
        return $this->role;
    }
    
    /**
     * @inheritDoc
     */
    public function getRoles() {
        return array($this->role);
    }
    
    
    public function getRoleByName(){
        
        return $this->all_roles[$this->role];
            
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials() {
        
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize() {
        return serialize(array(
                    $this->id,
                ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized) {
        list (
                $this->id,
                ) = unserialize($serialized);
    }

    public function __toString() {
        return $this->name . ' ' . $this->surname;
    }


    /**
     * Get isActive
     *
     * @return integer 
     */
    public function getIsActive()
    {
        return true;
    }
}