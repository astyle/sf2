
<?php if($ile>1):?>  
<?php 
$ile_przyciskow = ($showButtons > $ile)?$ile:$showButtons;
$pol = ceil($ile_przyciskow/2);

?>                        
         
<div class="pagination">
    <?php if ($p > 1): ?>
        <a class="prev pag" href="<?php echo $link ?>?p=<?php echo 1 ?>">&lsaquo;&lsaquo;</a>
        <a class="prev pag" href="<?php echo $link ?>?p=<?php echo $p - 1 ?>">&lsaquo;</a>
    <?php endif; ?>
                               
                               
    <?php for ($i = 1; $i <= $ile_przyciskow; $i++): ?>
    
        <?php
         if($p>$pol && $p < ($ile-$pol))
          $j = $i+($p-$pol);
         elseif($p >= $ile-$pol)
           $j = $i+($ile-$ile_przyciskow);
         else
          $j = $i;
        ?>
        
        <a class="pag <?php if($p == $j || ($j==1 && !isset($p))):?> current<?php endif;?>" href="<?php echo $link ?>?p=<?php echo $j ?>"><?php echo $j; ?></a>             
    
    
    <?php endfor; ?>
    
        
    <?php if ($p < ($ile)): ?>
        <a class="next pag" href="<?php echo $link ?>?p=<?php echo $p + 1 ?>">&rsaquo;</a>
        <a class="next pag" href="<?php echo $link ?>?p=<?php echo $ile ?>">&rsaquo;&rsaquo;</a>
    <?php endif; ?>
</div>
<?php endif;?>